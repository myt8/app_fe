import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import MainLayout from "./components/layout/MainLayout";
import { routes } from "./routes";
import CreateEmployee from "./service/employee/CreateEmployee";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainLayout />}>
          {routes}
        </Route>
        <Route path="/dashboard/adduser" element={<CreateEmployee />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
