import React, { useState } from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import axios from "axios";
import { useNavigate } from "react-router-dom";

interface Employee {
  firstName: string;
  lastName: string;
  email: string;
}

const CreateEmployee: React.FC = () => {
  let navigate = useNavigate();
  const [employee, setEmployee] = useState<Employee>({
    firstName: "",
    lastName: "",
    email: "",
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEmployee((prevEmployee) => ({
      ...prevEmployee,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    // Do something with the submitted employee data
    console.log("Submitted employee:", employee);
    e.preventDefault();
    await axios.post("http://localhost:8080/api/employees", employee);
    navigate("/");
    // Clear form fields after submission
    setEmployee({
      firstName: "",
      lastName: "",
      email: "",
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <Box
        display="flex"
        flexDirection="column"
        maxWidth="300px"
        mx="auto"
        my="2rem"
      >
        <TextField
          label="First Name"
          variant="outlined"
          name="firstName"
          value={employee.firstName}
          onChange={handleChange}
          margin="normal"
          required
        />
        <TextField
          label="Last Name"
          variant="outlined"
          name="lastName"
          value={employee.lastName}
          onChange={handleChange}
          margin="normal"
          required
        />
        <TextField
          label="Email"
          variant="outlined"
          name="email"
          value={employee.email}
          onChange={handleChange}
          margin="normal"
          required
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          sx={{ mt: 2 }}
        >
          Create Employee
        </Button>
      </Box>
    </form>
  );
};

export default CreateEmployee;
